Git Managed Directories
=======================

Description
-----------

This package includes a systemd service that manages directories with git. It consists of a shell script for syncing directories, 
a library of bash functions and environmental variables used by that script, so that other scripts can leverage components of the script, 
a systemd service template, and a systemd timer template. The systemd service template calls an EnvironmentFile per instance, which defines
which directory will be synced from which project, as well as details such as default users, permissions, git branches, and so on. The main service will pull, commit, and push, but alternate services that only pull or push are also provided.

The oneshot systemd service passes the environment info to the script, which then does the work of syncing the directory. For authentication, 
it relies on the default instance of the system-ssh-agent service. A corresponding timer template allows each instance of the service
to be run periodically. By default, the timer will start the corresponding service instance when it first launches, and the start it again 5 seconds
after the previous run completes. This ensures that the service instance only runs one copy at a time and does not overlap with itself, although 
multiple different instances can run in parallel.

The script itself uses etckeeper with an alternate config location to track file permissions. This allows file permissions to be set
remotely by changing the .etckeeper file. 

For security reasons, this depends on the system-ssh-agent service. It will join the namespace of that service so that it can read
the agent file in the private tmp folder of that service. However, the script could also be used with a different environment file
and a different ssh agent if desired outside of systemd.

