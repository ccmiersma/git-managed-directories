all: build



build:  src/git-managed-directories.sh.erb src/git-managed-directories@.service.erb docs/git-managed-directories.1.md
	mkdir build
	erb -r ./build-config.rb src/git-managed-directories-pull.sh.erb > build/git-managed-directories-pull.sh
	erb -r ./build-config.rb src/git-managed-directories-push.sh.erb > build/git-managed-directories-push.sh
	erb -r ./build-config.rb src/git-managed-directories.sh.erb > build/git-managed-directories.sh
	erb -r ./build-config.rb src/git-managed-directories-pull@.service.erb > build/git-managed-directories-pull@.service
	erb -r ./build-config.rb src/git-managed-directories-push@.service.erb > build/git-managed-directories-push@.service
	erb -r ./build-config.rb src/git-managed-directories@.service.erb > build/git-managed-directories@.service
	pandoc -s -t man docs/git-managed-directories.1.md | gzip > build/git-managed-directories.1.gz

install: README.md src/git-managed-directories.lib.sh src/default.conf build/git-managed-directories.sh build/*.service build/git-managed-directories.1.gz
	install -d -m 755 \
		./build/buildroot/usr/local/share/doc/git-managed-directories/ \
		./build/buildroot/usr/local/share/git-managed-directories/  \
		./build/buildroot/usr/local/libexec \
		./build/buildroot/etc/local/git-managed-directories/\
		./build/buildroot/usr/local/share/man/man1/ \
		./build/buildroot/etc/systemd/system/ 
	install -m 644 src/git-managed-directories.lib.sh ./build/buildroot/usr/local/share/git-managed-directories/ 
	install -m 644 src/default.conf ./build/buildroot/etc/local/git-managed-directories/
	install -m 644 README.md LICENSE ./build/buildroot/usr/local/share/doc/git-managed-directories/
	install -m 755 build/*.sh ./build/buildroot/usr/local/libexec/
	install -m 644 build/git-managed-directories.1.gz ./build/buildroot/usr/local/share/man/man1/
	install -m 644 build/*.service ./build/buildroot/etc/systemd/system/ 
	install -m 644 src/*.timer ./build/buildroot/etc/systemd/system/ 

clean:
	rm -rf build
