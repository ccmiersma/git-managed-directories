#This is just a library of environmental variables and functions please source this to use it.

export GIT_MANAGED_DIR_PATH=${GIT_MANAGED_DIR_PATH:-/tmp/git-managed-directories}
export ETCKEEPER_CONF_DIR=${ETCKEEPER_CONF_DIR:-/etc/etckeeper}
export GIT_MANAGED_DIR_DEFAULT_USER=${GIT_MANAGED_DIR_DEFAULT_USER:-root}
export GIT_MANAGED_DIR_DEFAULT_GROUP=${GIT_MANAGED_DIR_DEFAULT_GROUP:-root}
export GIT_MANAGED_DIR_DEFAULT_FILE_MODE=${GIT_MANAGED_DIR_DEFAULT_FILE_MODE:-0644}
export GIT_MANAGED_DIR_DEFAULT_DIR_MODE=${GIT_MANAGED_DIR_DEFAULT_DIR_MODE:-0755}
export GIT_MANAGED_DIR_GITLAB_SERVER=${GIT_MANAGED_DIR_GITLAB_SERVER:-gitlab.com} 
export GIT_MANAGED_DIR_GITLAB_GROUP=${GIT_MANAGED_DIR_GITLAB_GROUP:-ccmiersma} 
export GIT_MANAGED_DIR_GITLAB_PROJECT=${GIT_MANAGED_DIR_GITLAB_PROJECT:-git-managed-directories} 
export GIT_MANAGED_DIR_BRANCH=${GIT_MANAGED_DIR_BRANCH:-master}

function _die(){
	echo "Error while running $1"
	exit 1
}

function _git_clone(){
	command="mkdir -p $GIT_MANAGED_DIR_PATH"
	$command || _die "$command"

	chown $GIT_MANAGED_DIR_DEFAULT_USER:$GIT_MANAGED_DIR_DEFAULT_GROUP $GIT_MANAGED_DIR_PATH
	chmod $GIT_MANAGED_DIR_DEFAULT_DIR_MODE $GIT_MANAGED_DIR_PATH

	command="cd $GIT_MANAGED_DIR_PATH"
	$command || _die "$command"

	command="git clone git@$GIT_MANAGED_DIR_GITLAB_SERVER:$GIT_MANAGED_DIR_GITLAB_GROUP/$GIT_MANAGED_DIR_GITLAB_PROJECT.git ./"
	$command || _die "$command"

	command="git checkout $GIT_MANAGED_DIR_BRANCH" 
	$command || _die "$command"
	
	command="etckeeper init -d ./"
	$command || _die "$command"
	
	command="git push -u origin $GIT_MANAGED_DIR_BRANCH"
	$command || _die "$command"
}

function _git_set_permissions(){
        command="cd $GIT_MANAGED_DIR_PATH"
	$command || _die "$command"

	command="etckeeper init -d ./"
	$command || _die "$command"

        previously_known_files=$(cat .etckeeper | grep maybe | cut -f4 -d' ' | sed -e "s/'//g" | sort | uniq | grep -v "^\." )
        all_real_files=$(find ./* | sed 's/\.\///g' | sort | uniq)
        newfiles=$(diff <(echo $previously_known_files | tr ' ' '\n') <(echo $all_real_files | tr ' ' '\n') | grep "> " | sed 's/> //')

        for file_or_dir in $newfiles
        do
		chown $GIT_MANAGED_DIR_DEFAULT_USER:$GIT_MANAGED_DIR_DEFAULT_GROUP $file_or_dir
		test -f $file_or_dir && chmod $GIT_MANAGED_DIR_DEFAULT_FILE_MODE $file_or_dir
		test -d $file_or_dir && chmod $GIT_MANAGED_DIR_DEFAULT_DIR_MODE $file_or_dir
	done
}

function _git_push(){
	etckeeper commit -d ./ "Committed changes on $(hostname)"
	git push -u origin $GIT_MANAGED_DIR_BRANCH
}

function _git_ssh_setup(){
	source /tmp/system-ssh-agent-env.sh
}

